# Ansible Role: terraform-docs

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-terraform-docs/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-terraform-docs/-/commits/main)

This role installs [terraform-docs](https://github.com/terraform-docs/terraform-docs/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terraform_docs_version: latest # tag v0.15.0 if you want a specific version
    terraform_docs_arch: amd64 # amd64, arm or arm64
    terraform_docs_platform: linux # linux or freebsd
    setup_dir: /tmp
    terraform_docs_bin_path: /usr/local/bin/terraform-docs
    terraform_docs_repo_path: https://github.com/terraform-docs/terraform-docs/releases/download

This role can install the latest or a specific version. See [available terraform-docs releases](https://github.com/terraform-docs/terraform-docs/releases/) and change this variable accordingly.

    terraform_docs_version: latest # tag v0.15.0 if you want a specific version

The path of the terraform-docs repository.

    terraform_docs_repo_path: https://github.com/terraform-docs/terraform-docs/releases/download

The location where the terraform-docs binary will be installed.

    terraform_docs_bin_path: /usr/local/bin/terraform-docs

terraform-docs supports amd64, arm and arm64 CPU architectures, just change for the main architecture of your CPU.

    terraform_docs_arch: amd64 # amd64, arm arm64

terraform-docs needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install terraform-docs. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: terraform-docs

## License

MIT / BSD
